package ldacluster.lda.rlda;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import ldacluster.lda.rlda.data.Topic;
import ldacluster.lda.rlda.data.TopicNode;
import ldacluster.lda.rlda.util.RLDAConstants;
import ldacluster.lda.solr.SOLRservice;

/**
 * Main class of the RLDA 
 * Creates initial mapping of topics, then recursively generates topics of childn in a parallel/multithreaded manner
 *
 */
public class RecursiveLDA {
	static boolean staticLevels;
	final static Logger log = LoggerFactory.getLogger(RecursiveLDA.class);
	final static Set<String> stopwords = new HashSet<String>();
	static double ALPHA;
	static double BETA;
	static int OPT_INT;
	static int maxDepth;
	static boolean ignoreDupes;
	static String queryField;
	static String zkHostString;
	static String collection;
	static String kafkaBrokers;
	static String user;
	static String jobId;
	
	public static void main(String[] args){
		try {
			if (args.length !=1){
					log.error("Incorrect number of arguments, useage : 'config.file'");
					return;
				}
	
				long start = System.currentTimeMillis();
				log.debug("Start: " + new Date(start));
				Properties props = new Properties();
	
				SOLRservice solrService = new SOLRservice();
				TopicNodeService topicNodeService = new TopicNodeService();
	
				String config = args[0];
				if (config != null && config.length() > 0) {
					InputStream in;
					if (config.startsWith("hdfs")) { // HDFS resource
						Path pp = new Path(config);
						FileSystem fs = FileSystem.get(new Configuration());
						if (fs.exists(pp) && fs.isFile(pp))
							in = fs.open(pp);
						else
							throw new IOException("HDFS File '" + config + "' doesn't exists or is not a file.");
					} else {
						File pf = new File(config);
						if (pf.exists() && pf.isFile()) { // File system resource
							in = new FileInputStream(config);
						} else if (Properties.class.getClassLoader().getResource(config) != null) {
							in = Properties.class.getClassLoader().getResourceAsStream(config);
						} else
							throw new IOException(
									"File '" + config + "' doesn't exists or is not a file or a classpath resource.");
					}
					props.load(in);
				}
	
			setProps(props);
			getStopwords();
			
			log.debug("Setting up connection to " + zkHostString);
			solrService.setUpServer(zkHostString, queryField, ignoreDupes, kafkaBrokers);
	
			ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
			
			pipeList.add(new CharSequenceLowercase());
			pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
			pipeList.add(new TokenSequence2FeatureSequence());
			
			InstanceList instances = new InstanceList(new SerialPipes(pipeList));
			
			log.debug("Retrieving documents from SOLR collection " + collection);
			long retrievestart = System.currentTimeMillis();
			instances.addThruPipe(solrService.docsToInstances(RLDAConstants.BATCHSIZE, collection).iterator());
			log.debug("Time to retrieve : " + (System.currentTimeMillis()-retrievestart));
			
			int numInstances = instances.size();
			int numTopics = (int) Math.log((numInstances*0.05));
			numTopics = numTopics>1?numTopics:1;
			log.debug("Performing LDA with " + numTopics + " topics");
			TopicGenerator.setNumTopics(numTopics);
			ParallelTopicModel model = new ParallelTopicModel(numTopics, ALPHA, BETA);
			model.setOptimizeInterval(OPT_INT);
			model.addInstances(instances);
			model.setNumThreads(RLDAConstants.MAINTHREAD);
			model.setNumIterations(RLDAConstants.NUMITER);
			model.estimate();
	
			Alphabet dataAlphabet = instances.getDataAlphabet();
	
	    	ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
			Map<Integer, Topic> topicIdNameMap = new HashMap<>();
			Map<Integer, List<Instance>> topicInstanceMap = new HashMap<Integer, List<Instance>>();
			
			topicNodeService.getTopicIdNameMap(topicIdNameMap, numTopics, topicSortedWords, dataAlphabet, stopwords);
	
	    	int iteration = 0;
	    	List<TopicNode> topicNodeList = Collections.synchronizedList(new ArrayList<TopicNode>());
			topicNodeService.instance2Map(topicInstanceMap, instances, model, numTopics);
			List<TopicNode> nodes = topicNodeService.topicNodeToClusters(topicInstanceMap, topicIdNameMap, maxDepth);
			ExecutorService threadPool = Executors.newFixedThreadPool(RLDAConstants.TPSIZE);
			
			TopicGenerator tg = new TopicGenerator(topicNodeList, iteration, maxDepth, numInstances, nodes, threadPool);
			Thread treeRunner = new Thread(tg);
			Future<?> future = threadPool.submit(treeRunner);
			while (!future.isDone()){}
			threadPool.shutdown();
			
			if (!topicNodeList.isEmpty()) {
				synchronized(topicNodeList){
					solrService.solrUpdate(topicNodeList, collection, user);
				}
			}
			//Testing only: writes output to a file
//			File fileTest = new File("");
//			BufferedWriter writer = new BufferedWriter(new FileWriter(fileTest));
//			for (TopicNode cluster : topicNodeList) {
//				writer.write(cluster.toString());
//				for (Instance instance : cluster.getNodes()) {
//					writer.newLine();
//					writer.write("\t\t\t" + instance.getName());
//				}
//				writer.newLine();
//			}
//			writer.close();
			solrService.shutDown();
			log.debug("Total clusters : " + topicNodeList.size());
			long time = System.currentTimeMillis()-start;
			log.debug("Time : " + (time/60/1000) + " minutes");
		}
		catch(Exception ex) {
			log.error("Error caused by " + ex.getMessage());
			
		}
	}

	/**
	 * Set up list of stopwords
	 * @throws IOException
	 */
	private static void getStopwords() throws IOException {
		ClassPathResource topic_stopwords = new ClassPathResource("topic_stopwords.txt");
		try (BufferedReader bif = new BufferedReader(new InputStreamReader(topic_stopwords.getInputStream()))){
			String line = bif.readLine();
			while (line !=null) {
				stopwords.add(line.trim());
				line = bif.readLine();
			}
		}
	}
	

	/**
	 * Set up properties
	 * @param props
	 */
	private static  void setProps(Properties props) {
		user = props.getProperty("user");
		collection = props.getProperty("solr.collection");
		zkHostString = props.getProperty("zk.hosts");
		maxDepth = Integer.parseInt(props.getProperty("depth"));
		queryField = props.getProperty("queryfield");
		ALPHA = Double.parseDouble(props.getProperty("alpha"));
		BETA = Double.parseDouble(props.getProperty("beta"));
		OPT_INT= Integer.parseInt(props.getProperty("optimizeInterval"));
		ignoreDupes = Boolean.parseBoolean(props.getProperty("ignoredupes"));
		staticLevels = Boolean.parseBoolean(props.getProperty("staticLevels"));
		jobId = props.getProperty("jobId");
		String kafka = null;
		String tkeySerializer = "";
		String tvalueSerializer = "";
		Map<String, Object> kafkaProps = new HashMap<>();
		if ((kafka = props.getProperty("bootstrap.servers", "")) != null && !kafka.isEmpty()){
			kafkaProps.put("bootstrap.servers", kafka);
			kafkaBrokers = kafka;
		}
		else if ((kafka = props.getProperty("kafka.brokers", "")) != null && !kafka.isEmpty()) {
			kafkaProps.put("bootstrap.servers", kafka);
			kafkaBrokers = kafka;
		}
		else {
			throw new IllegalArgumentException("Kafka brokers required for RLDA");
		}
		if ((tkeySerializer = props.getProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")) != null && !tkeySerializer.isEmpty()){
			kafkaProps.put("key.serializer", tkeySerializer);
		}
		if ((tvalueSerializer = props.getProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")) != null && !tvalueSerializer.isEmpty()){
			kafkaProps.put("value.serializer", tvalueSerializer);
		}
	}
	
}
