package ldacluster.lda.rlda;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.mallet.pipe.Noop;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import ldacluster.lda.rlda.data.Topic;
import ldacluster.lda.rlda.data.TopicNode;
import ldacluster.lda.rlda.util.RLDAConstants;

/**
 * Class performing the majority of the parallelization and recursion for topic
 * clustering
 *
 */
public class TopicGenerator implements Runnable {
	private static Logger log = LoggerFactory.getLogger(TopicGenerator.class);
	static int numTopics;
	List<TopicNode> topicNodeList;
	int numDocs;
	List<TopicNode> topicNodes;
	int numIteration;
	int maxDepth;
	ExecutorService executor;
	TopicNodeService topicNodeService;

	public TopicGenerator(List<TopicNode> topicNodeList, int numIteration, int maxDepth, int numDocs,
			List<TopicNode> topicNodes, ExecutorService executor) {
		this.topicNodeList = topicNodeList;
		this.numIteration = numIteration;
		this.maxDepth = maxDepth;
		this.topicNodes = topicNodes;
		this.numDocs = numDocs;
		this.executor = executor;
	}

	public static void setNumTopics(int numTopics) {
		TopicGenerator.numTopics = numTopics;
	}

	@Override
	public void run() {
		try {

			if (numIteration < maxDepth || !RecursiveLDA.staticLevels) {
				log.debug("Performing LDA on level " + numIteration);
				topicNodeService = new TopicNodeService();
				List<Future<?>> futures = new ArrayList<Future<?>>();
				int numValidNodes = 0;
				for (TopicNode node : topicNodes) {
					if (node.getNodes().size() >= 250) {
						numValidNodes++;
					}
				}
				if (numValidNodes > 1) {
					for (TopicNode node : topicNodes) {

						Topic[] topicsInTopicNode = node.getTopics();
						List<Instance> instanceList = node.getNodes();
						log.debug("Topic " + node.getTopicsAsString() + " has " + instanceList.size() + " documents, "
								+ numDocs + " documents in parent topic");
						if (instanceList.size() >= 250) {
							ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
							pipeList.add(new Noop());
												
							InstanceList instances = new InstanceList(new SerialPipes(pipeList));
							instances.addThruPipe(instanceList.iterator());
							int numTopics = (int) Math.log((instanceList.size() * 0.05));
							numTopics = numTopics > 1 ? numTopics : 1;
							log.debug("Performing LDA on level " + numIteration + " with " + numTopics
									+ " topics, documents for this level : " + instanceList.size());
							ParallelTopicModel model = new ParallelTopicModel(numTopics, RecursiveLDA.ALPHA,
									RecursiveLDA.BETA);
							model.setOptimizeInterval(RecursiveLDA.OPT_INT);
							model.addInstances(instances);
							model.setNumThreads(RLDAConstants.CHILDTHREAD);
							model.setNumIterations((numIteration + 1) * 250);
							
							try {
								model.estimate();
							} catch (IOException e) {
								log.error("Error creating topic model : " + e.getMessage());
							}

							Alphabet dataAlphabet = instances.getDataAlphabet();

							Map<Integer, Topic> topicIdNameMap = new HashMap<Integer, Topic>();
							ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
							Map<Integer, List<Instance>> topicInstanceMap = new HashMap<Integer, List<Instance>>();

							topicMapping(node, numTopics, dataAlphabet, topicIdNameMap, topicSortedWords);
							topicNodeService.instance2Map(topicInstanceMap, instances, model, numTopics);
							List<TopicNode> tempCluster = topicNodeToClusters(topicInstanceMap, topicIdNameMap,
									numIteration, topicsInTopicNode);
							TopicGenerator tg = new TopicGenerator(topicNodeList, (numIteration + 1), maxDepth,
									instances.size(), tempCluster, executor);
							Future<?> future = executor.submit(tg);
							futures.add(future);
							
						} else {
							log.debug("Document threshold not met for " + node + ", only " + instanceList.size()
									+ " docs");
							topicNodeList.add(node);
						}
					}

				} else {
					log.debug("Less than 2 valid nodes...");
					for (TopicNode node : topicNodes) {
						topicNodeList.add(node);
					}
				}

				for (Future<?> future : futures) {
					try {
						future.get();
					} catch (Exception e) {
						e.printStackTrace();
						log.error(e.getMessage());
						for (StackTraceElement ste : e.getStackTrace()) {
							log.error(ste.getClassName() + " " + ste.getMethodName() + " " + ste.getLineNumber());
						}
					}
				}
				log.debug("Done processing level " + numIteration);
			} else {
				log.debug("Depth " + numIteration + " reached ");
				for (TopicNode clusterNode : topicNodes) {
					topicNodeList.add(clusterNode);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	private void topicMapping(TopicNode node, int numTopics, Alphabet dataAlphabet, Map<Integer, Topic> topicIdNameMap,
			ArrayList<TreeSet<IDSorter>> topicSortedWords) {
		for (int topic = 0; topic < numTopics; topic++) {
			Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();
			int rank = 0;
			List<String> termsies = new ArrayList<>();
			List<Double> weightsies = new ArrayList<>();
			String[] terms = null;
			Double[] weight = null;
			int termsPerTopic = RLDAConstants.TERMS_PER_TOPIC;
			while (iterator.hasNext() && rank < termsPerTopic) {
				IDSorter idCountPair = iterator.next();
				String term = (String) dataAlphabet.lookupObject(idCountPair.getID());
				if (!RecursiveLDA.stopwords.contains(term) && term.length() > 1) {
					term = term.replaceAll("(_noun)|(_adjective)", "");
					termsies.add(term);
					weightsies.add(idCountPair.getWeight());
					rank++;
					if (node.containsTerm(term)) {
						termsPerTopic++;
						termsies.remove(term);
					}
				}
				terms = termsies.toArray(new String[termsies.size()]);
				weight = weightsies.toArray(new Double[weightsies.size()]);
			}
			Topic tempTopic = new Topic(topic, terms);
			tempTopic.setWeights(weight);
			topicIdNameMap.put(topic, tempTopic);
		}
	}

	/**
	 * Transforms initial map of topics and instances into a list of topic nodes
	 * 
	 * @param topicMap
	 * @param topicIdMap
	 * @param numIteration
	 * @param currTopics
	 * @return
	 */
	public List<TopicNode> topicNodeToClusters(Map<Integer, List<Instance>> topicMap, Map<Integer, Topic> topicIdMap,
			int numIteration, Topic[] currTopics) {
		List<TopicNode> topicClusters = new ArrayList<TopicNode>();
		if (topicMap == null || topicIdMap == null) {
			topicClusters = null;
		} else {
			for (Entry<Integer, List<Instance>> cluster : topicMap.entrySet()) {
				Topic tempTopic = new Topic();
				tempTopic = topicIdMap.get(cluster.getKey());
				if (tempTopic != null) {
					try {
						TopicNode topicNode = new TopicNode(currTopics);
						topicNode.addNodeLevel(numIteration + 1, tempTopic);
						topicNode.setNodes(cluster.getValue());
						topicClusters.add(topicNode);
					} catch (Exception ex) {
						//debug
//						log.error("Error with topic " + tempTopic + ex.getMessage() + " , current topics : ");
//						for (Topic topic : currTopics) {
//							log.error(topic.toString());
//						}
//						for (StackTraceElement ste : ex.getStackTrace()) {
//							log.error(ste.getClassName() + " : " + ste.getMethodName() + " : " + ste.getLineNumber());
//						}
					}
				}

			}
		}
		return topicClusters;
	}

	/**
	 * Returns aggregated list of topic nodes
	 * 
	 * @return
	 */
	public List<TopicNode> getTopicNodeList() {
		return topicNodeList;
	}

}
