package ldacluster.lda.rlda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import ldacluster.lda.rlda.data.Topic;
import ldacluster.lda.rlda.data.TopicNode;
import ldacluster.lda.rlda.util.RLDAConstants;

/**
 * Class that provides helper methods to help transform/manipulate the data
 * so additional levels of LDA can be performed
 * 
 */
public class TopicNodeService {
	
	/**
	 * 
	 * @param topicIdNameMap
	 * @param numTopics
	 * @param topicSortedWords
	 * @param dataAlphabet
	 * @param stopwords
	 */
	public void getTopicIdNameMap (Map<Integer, Topic> topicIdNameMap, int numTopics, ArrayList<TreeSet<IDSorter>> topicSortedWords, Alphabet dataAlphabet, Set<String> stopwords){
		for (int topic = 0; topic < numTopics; topic++) {
    		Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();
    		int rank = 0;
    		String[] terms = new String[RLDAConstants.TERMS_PER_TOPIC];
    		Double [] weight = new Double[RLDAConstants.TERMS_PER_TOPIC];
    		while (iterator.hasNext() && rank < RLDAConstants.TERMS_PER_TOPIC) {
    			IDSorter idCountPair = iterator.next();
    			String term = (String) dataAlphabet.lookupObject(idCountPair.getID());
    			if (!stopwords.contains(term) && term.length() > 1){
    				terms[rank] = term;
    				weight[rank] = idCountPair.getWeight();
        			rank++;
    			}
    		}
    		Topic tempTopic = new Topic(topic, terms);
    		tempTopic.setWeights(weight);
    		topicIdNameMap.put(topic, tempTopic);
    	}
	}
	
	/**
	 * Transforms initial map of topics and instances into a list of topic nodes
	 * @param topicMap
	 * @param topicIdMap
	 * @param topicCount
	 * @return
	 */
	public List<TopicNode> topicNodeToClusters(Map<Integer,List<Instance>> topicMap, Map<Integer, Topic> topicIdMap, int topicCount) {
		List<TopicNode> topicClusters = new ArrayList<TopicNode>();
		if (topicMap == null || topicIdMap == null) {
			topicClusters = null;
		}
		else {
			for (Entry<Integer, List<Instance>> cluster : topicMap.entrySet()) {
				Topic tempTopic = topicIdMap.get(cluster.getKey());
				if (tempTopic != null) {
					TopicNode topicNode = new TopicNode(topicCount+1);
					topicNode.addNodeLevel(0, tempTopic);
					topicNode.setNodes(cluster.getValue());
					topicClusters.add(topicNode);
				}
			}
		}
		return topicClusters;
	}
	
	public void instance2Map(Map<Integer, List<Instance>> topicInstanceMap, InstanceList instances, ParallelTopicModel model, int numTopics){
		Iterator<Instance> instIter = instances.iterator();
		int index = 0;
		while(instIter.hasNext()) {
			Instance instance = instIter.next();
			double[] topicDistribution = model.getTopicProbabilities(index);
			double highChance = -0d;
			int topicId = -1;
			for (int topic = 0; topic < numTopics; topic++) {
				double currChance = topicDistribution[topic];
				if ( currChance > highChance) {
					highChance = currChance;
					topicId = topic;
				}
			}
			addInstanceToMap(topicInstanceMap, instance, topicId);
			index++;
		}
	}
	
	/**
	 * Adds an instance associated with a topic to a mapping of topics-instances
	 * @param intStanceMap
	 * @param instance
	 * @param topic
	 */
	public void addInstanceToMap(Map<Integer, List<Instance>> intStanceMap, Instance instance, int topic) {
		List<Instance> instanceList = intStanceMap.get(topic);
		if (instanceList == null) {
			instanceList = new ArrayList<Instance>();
		}
		instanceList.add(instance);
		intStanceMap.put(topic, instanceList);
	}
}
