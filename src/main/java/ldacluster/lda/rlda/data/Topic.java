package ldacluster.lda.rlda.data;

/**
 *	Class representing a topic : id that Mallet LDA assigns to it
 *	as well as the first 3 most probable terms in the topic 
 *
 */
public class Topic {

	private int id;
	private String[] terms;
	private Double[] weights;
	
	public Topic (){}
	
	public Topic(int id, int numTerms){
		this.id = id;
		terms = new String[numTerms];
	}
	
	public Topic(int id, String[] terms){
		this.id = id;
		this.terms = terms;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String[] getTerms() {
		return terms;
	}
	public void setTerms(String[] terms) {
		this.terms = terms;
	}
	
	public Double[] getWeights() {
		return weights;
	}

	public void setWeights(Double[] weight) {
		this.weights = weight;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (String term : terms){
			sb.append(term);
			sb.append(' ');
		}
		return sb.toString().trim();
	}
	
	public String toStringWithWeights() {
		StringBuilder sb = new StringBuilder();
		for (int index = 0; index < terms.length; index++){
			
			sb.append(terms[index]);
			sb.append(':');
			sb.append(weights[index]);
			sb.append(' ');
		}
		return sb.toString().trim();
	}
}
