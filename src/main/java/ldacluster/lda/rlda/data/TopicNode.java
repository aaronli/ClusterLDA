package ldacluster.lda.rlda.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cc.mallet.types.Instance;

/**
 * Class that represents a set of data that has undergone LDA, contains the 
 * hierarchy of topics and the subset of documents in the final subtopic 
 *
 */
public class TopicNode {

	private Topic[] topics;
	List<Instance> nodes;
	Set<String> terms;
	
	public TopicNode (){}
	
	public TopicNode(int numTopics){
		topics = new Topic[numTopics];
		nodes = new ArrayList<Instance>();
		terms = new HashSet<String>();
	}
	public TopicNode(Topic[] topics){
		terms = new HashSet<String>();
		setTopics(topics);
		nodes = new ArrayList<Instance>();
	}
	public TopicNode(Topic[] topics, List<Instance> nodes){
		terms = new HashSet<String>();
		setTopics(topics);
		this.nodes = nodes;
	}
	
	public Topic[] getTopics() {
		return topics;
	}
	public void setTopics(Topic[] topics) {
		Topic[] temp = new Topic[topics.length];
		for (int index = 0; index < topics.length; index++){
			Topic topic = topics[index];
			if (topic != null){
			for (String term : topic.getTerms()){
				terms.add(term);
			}
			temp[index] = topic;
			}
		}
		this.topics = temp;
	}
	public List<Instance> getNodes() {
		return nodes;
	}
	public void setNodes(List<Instance> nodes) {
		this.nodes = nodes;
	}
	public void addNodeLevel(int nodeLevel, Topic topic){
		if (!(nodeLevel < topics.length)){
			Topic[] temp = topics;
			topics = new Topic[topics.length+1];
			for (int index = 0; index < temp.length; index++){
				topics[index] = temp[index];
			}
		}
		topics[nodeLevel] = topic;
		for (String term : topic.getTerms()){
			terms.add(term);
		}
	}
	
	public boolean containsTerm(String word) {
		return terms.contains(word);
	}
	
	/**
	 * Turning a set of topics into a single string
	 * @return
	 */
	public String getTopicsAsString() {
		StringBuilder topicString = new StringBuilder();
		for (Topic topic : topics) {
			if (topic == null){
				break;
			}
			topicString.append(topic);
			topicString.append('|');
		}
		return topicString.substring(0, topicString.length()-1).toString();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("\nInstances in topic: ");
		sb.append(nodes.size());
		sb.append("\nTopic Hierarchy: ");
		for (int i = 0; i < topics.length; i++){
			Topic topic = topics[i];
			if (topic!=null){
				sb.append("\n");
				for (int j = 0; j < i; j++){
					sb.append('\t');
				}
				sb.append(topic.toStringWithWeights());
			}
		}
		return sb.toString();
	}
}