package ldacluster.lda.rlda.util;

public class RLDAConstants {

	public static final int BATCHSIZE = 5000;
	public static final int TPSIZE = 200;
	public static final int NUMITER = 100;
	public static final int MAINTHREAD = 10;
	public static final int CHILDTHREAD = 3;
	public static final int TERMS_PER_TOPIC = 3;
	
}
