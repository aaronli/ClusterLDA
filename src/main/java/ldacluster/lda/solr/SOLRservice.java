package ldacluster.lda.solr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.SortClause;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CursorMarkParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.mallet.types.Instance;
import ldacluster.lda.rlda.data.TopicNode;

/**
 * Class to interface with SOLR
 *
 */
public class SOLRservice {
	
	private static final Logger log = LoggerFactory.getLogger(SOLRservice.class);
	private CloudSolrClient server;
	private String queryField;
	private boolean ignoreDupes;
	private static final int SOLR_BATCHSIZE = 5000;
	
	public void setUpServer(String zkHostString, String queryField, boolean ignoreDupes, String kafkaServers) {
		this.server = new CloudSolrClient(zkHostString);
		this.queryField = queryField;
		this.ignoreDupes = ignoreDupes;
	}
	
	public void shutDown(){
		if (server != null){
			try {
				server.commit();
				server.close();
			} catch (IOException | SolrServerException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Query solr for document parsedtext from a specific collection, and transform
	 * the data into instances 
	 * @param numDocs
	 * @param collection
	 * @return
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public List<Instance> docsToInstances(int numDocs, String collection) throws SolrServerException, IOException
	{
		if (server == null) {
			throw new IllegalArgumentException("Server has not been created yet");
		}
		List<Instance> instances = new ArrayList<Instance>();

		server.setDefaultCollection(collection);
		String solrQueryAll = queryField+":[* TO *]";
		SolrQuery query = new SolrQuery(solrQueryAll);
		query.setFields(queryField,"guid").setRows(numDocs).setSort(SortClause.asc("guid"));
		if (ignoreDupes){
			query.addFilterQuery("-textdupeof:*");
		}
		boolean done = false;
		String cursor = CursorMarkParams.CURSOR_MARK_START;
		while (!done){
			QueryResponse response = null;

			query.set(CursorMarkParams.CURSOR_MARK_PARAM, cursor);
			response = server.query(query);
			String nextCursor = response.getNextCursorMark();

			SolrDocumentList responseList = response.getResults();
			for (SolrDocument solrDoc : responseList){
				String guid = (String)solrDoc.get("guid");
				String data = (String)solrDoc.get(queryField);
				if (data!=null){
					if (data.length() > 100 && data.length() < 100000){
						Instance solrInst = new Instance(data, null, guid, null);
						instances.add(solrInst);
					}
				}
			}

			if(cursor.equals(nextCursor)){
				done = true;
			}
			cursor = nextCursor;
			log.debug("Current SOLR list size : " + instances.size());
		}
		log.debug("Total SOLR Docs : " + instances.size());
		return instances;
	}
	
	/**
	 * Update SOLR collection with categories for documents
	 * @param topicNodeList
	 * @param collection
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public void solrUpdate(List<TopicNode> topicNodeList, String collection, String user) throws SolrServerException, IOException {
		//add updated topic clusters to solr docs
		server.setDefaultCollection(collection);
		List<SolrInputDocument> solrDocs = new ArrayList<SolrInputDocument>();
		for (TopicNode cluster : topicNodeList) {
			for (Instance inst : cluster.getNodes()) {
				String guid = (String) inst.getName();
				SolrInputDocument doc = new SolrInputDocument();
				doc.addField("guid", guid);
				Map<String, Object> categories = new HashMap<String, Object>();
				categories.put("set", cluster.getTopicsAsString());
				
				doc.addField("Category", categories);
				solrDocs.add(doc);
				if(solrDocs.size() >= SOLR_BATCHSIZE) {
					submitDocs(solrDocs);
				}
			}
		}
		submitDocs(solrDocs);
	}
	
	/**
	 * Batch update to solr
	 * @param solrDocs
	 * @throws SolrServerException
	 * @throws IOException
	 */
	private void submitDocs(List<SolrInputDocument> solrDocs) throws SolrServerException, IOException{
		server.add(solrDocs);
		log.debug("Added " + solrDocs.size() + " docs");
		solrDocs.clear();
	}
	
}
